package stepsDefinition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.DashboardPage;
import objects.Hooks;
import objects.LoginPage;

public class LoginStepsDefinition {

	public WebDriver driver;
	public LoginPage loginPageObj = new LoginPage(null);
	public Hooks hooksObj = new Hooks(driver);
	public DashboardPage dashPageObj = new DashboardPage(driver);

	@Given("I have open the browser")
	public void openBrowser() {
		hooksObj.testSetUp();
		driver.findElement(By.xpath("//a[@class=\"tools-icon my-account-icon \"]")).click();

	}

	@When("^User enter (.*) and (.*)$")
	public void insertValidUsernameAndPassword(String username, String password) {
		loginPageObj.setUsernameOrEmailAddressInput(username);
		loginPageObj.setloginPasswordInput(password);
		loginPageObj.clickOnLoginButton();

	}

	@Then("User is redirected to Dashboard")
	public void user_is_redirected_to_dashboard() throws InterruptedException {
		Boolean dashboard = dashPageObj.verifyDashboardLinkLeftMenu();
		assertTrue(dashboard);
		hooksObj.tesTearDown();
	}

	@When("^User enters invalid (.*) and invalid (.*)$")
	public void insertInvalidUsernameAndPassword(String username, String password) {
		loginPageObj.setUsernameOrEmailAddressInput(username);
		loginPageObj.setloginPasswordInput(password);
		loginPageObj.clickOnLoginButton();

	}

	@Then("Error allert should be shown")
	public void errorAllert() throws InterruptedException {
		Boolean isDisplayed = driver.findElement(By.xpath("//ul[@class='woocommerce-error']")).isDisplayed();
		assertTrue(isDisplayed);
		hooksObj.tesTearDown();
	}

	@When("User click on login button without entering username and password")
	public void clickOnLoginButton() {
		loginPageObj.clickOnLoginButton();
	}
	@Then("Error allert should be displayed")
	public void usernameIsRequired() throws InterruptedException {
	  Boolean  error = driver.findElement(By.xpath("//li//strong")).isDisplayed();
	   
	    assertTrue(error);  
	    hooksObj.tesTearDown();
	}
	@When("^User leave empty (.*) and type valid (.*)$")
	public void insertValidPasswordWithEmptyUsername(String username, String password) {
		loginPageObj.setUsernameOrEmailAddressInput(username);
		loginPageObj.setloginPasswordInput(password);
		loginPageObj.clickOnLoginButton();
		
	}
	@When("^User enter valid (.*) and leave empty (.*)$")
	public void insertValidUsernameWithEmptyPassword(String username, String password) {
		loginPageObj.setUsernameOrEmailAddressInput(username);
		loginPageObj.setloginPasswordInput(password);
		loginPageObj.clickOnLoginButton();
		
	}

	@When("User click on \"Lost your password?\" link")
	public void clickOnLostYourPasswordLink() {
		loginPageObj.clickOnLostPasswordLink();
		
	}
	@Then("User should be redirected to a page for reseting password")
	public void verifyUserIsOnResetingPasswordPage() throws InterruptedException {
		 Boolean resetPasswordButton = driver.findElement(By.xpath("//button[text()=\"Reset password\"]")).isDisplayed();
		    assertTrue(resetPasswordButton);
		    hooksObj.tesTearDown();
	}
}
