package tests;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/",
		glue = {"stepsDefinitions" },
		plugin = {"pretty","junit:target/Reports/report.xml"}
		)


public class TestRunner {

}
