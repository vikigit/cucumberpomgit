package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	 public WebDriver driver;

	// By Locators:

	By loginUsernameOrEmailAddressInput = By.xpath("//input[@id='username']");
	By loginPasswordInput = By.xpath("//input[@id='password']");
	By loginButton = By.xpath("//button[@name='login']");
	By lostYourPasswordLinkText = By.xpath("//a[text()='Lost your password?']");
	
	// constructor of the page class:
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		//PageFactory.initElements(driver, this);   //check this

	}
	
	// page actions: features(behavior) of the page
	
		public String getLoginPageTitle() {
			return driver.getTitle();
		}
		public void clickOnLostPasswordLink() {
			driver.findElement(lostYourPasswordLinkText).click();
		}
				
		
		public boolean verifyLostYourPasswordLinkText() {
			return driver.findElement(lostYourPasswordLinkText).isDisplayed();
		}
		
		public void setUsernameOrEmailAddressInput(String username) {
			driver.findElement(loginUsernameOrEmailAddressInput).sendKeys(username);
		}
		public void setloginPasswordInput(String password) {
			driver.findElement(loginPasswordInput).sendKeys(password);
		}
		
		public void clickOnLoginButton() {
			driver.findElement(loginButton).click();
		}
		public void loginValidUser(String username, String password) {
			driver.findElement(loginUsernameOrEmailAddressInput).sendKeys(username);
			driver.findElement(loginPasswordInput).sendKeys(password);
			driver.findElement(loginButton).click();
		}
}
