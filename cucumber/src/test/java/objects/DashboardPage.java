package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {
	public WebDriver driver;

	// By Locators:
    By iconDashboardLinkLeftMenu = By.xpath("//a//child::span[@class=\"woostify-svg-icon icon-dashboard\"]");

	// constructor of the page class:
	public DashboardPage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean verifyDashboardLinkLeftMenu() {
		return driver.findElement(iconDashboardLinkLeftMenu).isDisplayed();
	}

}
