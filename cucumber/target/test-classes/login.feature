Feature: Login functionality

  Scenario Outline: Login with valid credentials
    Given I have open the browser
    When User enter <username> and <password>
    Then User is redirected to Dashboard

    Examples: 
      | username | password          |
      | user1    | testinginjira2022 |

  Scenario Outline: Login with invalid credentials
    Given I have open the browser
    When User enters invalid <username> and invalid <password>
    Then Error allert should be shown

    Examples: 
      | username | password |
      | .#4%     | FFgHH    |

  Scenario: Login with empty credentials
    Given I have open the browser
    When User click on login button without entering username and password
    Then Error allert should be displayed

  Scenario Outline: Login with empty username and valid password
    Given I have open the browser
    When User leave empty <username> and type valid <password>
    Then Error allert should be displayed

    Examples: 
      | username | password          |
      |          | testinginjira2022 |

  Scenario Outline: Login with valid username and empty password
    Given I have open the browser
    When User enter valid <username> and leave empty <password>
    Then Error allert should be displayed

    Examples: 
      | username | password |
      | user1    |          |

  Scenario: Verify "Lost your password?" is available link
    Given I have open the browser
    When User click on "Lost your password?" link
    Then User should be redirected to a page for reseting password
